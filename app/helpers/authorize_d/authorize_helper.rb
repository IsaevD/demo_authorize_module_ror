module AuthorizeD
  module AuthorizeHelper

    # Начать сеанса работы пользователя с системой
    def sign_in(entity, item, remember_token_param)
      remember_token = new_remember_token
      cookies.permanent[remember_token_param] = remember_token
      item.update_attribute(remember_token_param, encrypt_token(remember_token))
    end

    # Завершить сеанса работы пользователя с системой
    def sign_out(entity, remember_token_param)
      current_user(entity, remember_token_param).update_attribute(remember_token_param, encrypt_token(new_remember_token))
      cookies.delete(remember_token_param)
    end

    # Проверить, начат ли сеанс пользователя с системой
    def signed_in(user)
      !user.nil?
    end

    # Получить текущего пользователя системы
    def current_user(entity, remember_token_param)
      remember_token = encrypt_token(cookies[remember_token_param])
      return entity.find_by(remember_token_param => remember_token)
    end

    # Авторизовать пользователя в системе
    def authorize_user(entity, remember_token_param, user, password)
      if user && user.authenticate(password)
        sign_in(entity, user, remember_token_param)
        return true
      else
        return false
      end
    end

    # Сгененрировать ссылку выхода из системы
    def generate_sign_out_link(user, path)
      if signed_in(user)
        return link_to t('labels.buttons.exit'), path, method: "delete"
      end
    end

    private

      # Проверить, авторизован ли пользователь
      def signed_in_user(path, user)
        redirect_to path unless signed_in(user)
      end

      def new_remember_token
        return SecureRandom.urlsafe_base64
      end

      def encrypt_token(token)
        return Digest::SHA1.hexdigest(token.to_s)
      end

  end
end
