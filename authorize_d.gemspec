$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "authorize_d/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "authorize_d"
  s.version     = AuthorizeD::VERSION
  s.authors     = "Denis Isaev"
  s.email       = "isaevdenismich@mail.ru"
  s.homepage    = "http://www.den-isaev.com"
  s.summary     = "Модуль авторизации пользователей в системе"
  s.description = "Универсальный модуль авторизации пользователей в системе"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.1.0"

  s.add_development_dependency "sqlite3"
end
