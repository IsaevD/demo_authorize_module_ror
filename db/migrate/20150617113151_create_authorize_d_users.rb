class CreateAuthorizeDUsers < ActiveRecord::Migration
  def change
    create_table :authorize_d_users do |t|
      t.string :login
      t.string :password_digest
      t.string :remember_token
      t.timestamps
    end
  end
end
