module AuthorizeD
  class AuthUserController < ApplicationController

    before_filter :init_variables

    def new
      if signed_in(current_user(@entity, @remember_token_name))
        redirect_to @path
      end
    end

    def create
      user = @entity.find_by(@login_param => params[@param_name][@login_param])
      password = params[@param_name][@password_param]
      if authorize_user(@entity, @remember_token_name, user, password)
        redirect_to @path
      else
        flash[:error] = 'Неверное имя пользователя/пароль'
        render "new"
      end
    end

    def destroy
      sign_out(@entity, @remember_token_name)
      redirect_to @path
    end

    private

      def init_variables
        @entity = User
        @path = authorize_d.users_path
        @remember_token_name = :remember_token
        @param_name = :auth_user
        @login_param = :login
        @password_param = :password
      end

  end
end