AuthorizeD::Engine.routes.draw do
  resources :users
  resources :system_users

  match "/auth_user",  to: "auth_user#new",     via: "get"
  match "/auth_user",  to: "auth_user#create",     via: "post"
  match "/auth_user",  to: "auth_user#destroy", via: "delete"

  match "/auth_system_user",  to: "auth_system_user#new",     via: "get"
  match "/auth_system_user",  to: "auth_system_user#create",     via: "post"
  match "/auth_system_user",  to: "auth_system_user#destroy", via: "delete"

end
