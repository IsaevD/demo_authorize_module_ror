module AuthorizeD
  class UsersController < ApplicationController
    before_filter :init_variables

    def index
      all_item(@entity)
    end

    def show
      get_item_by_id(@entity, params[:id])
    end

    def new
      new_item(@entity)
    end

    def edit
      get_item_by_id(@entity, params[:id])
    end

    def create
      create_item(@entity, @path, prepared_params(@param_name, @params_array))
    end

    def update
      update_item(@entity, @path, params[:id], prepared_params(@param_name, @params_array))
    end

    def destroy
      delete_item(@entity, @path, params[:id])
    end

    private

      def init_variables
        @entity = User
        @param_name = :user
        @params_array = [:login, :password, :password_confirmation]
        @path = authorize_d.users_path
      end

  end
end
