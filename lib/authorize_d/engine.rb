module AuthorizeD
  class Engine < ::Rails::Engine
    isolate_namespace AuthorizeD
  end
end
